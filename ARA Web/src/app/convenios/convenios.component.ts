import { Component, OnInit } from '@angular/core';
import { ObraSocial } from '../interfaces/obra-social';
import { ObraSocialService } from '../services/obra-social.service';
import { UserService } from '../services/user.service';
import { NgxSpinnerService } from "ngx-spinner";
import { ConveniosService } from '../services/convenios.service';

@Component({
  selector: 'app-convenios',
  templateUrl: './convenios.component.html',
  styleUrls: ['./convenios.component.css']
})
export class ConveniosComponent implements OnInit {

  obras_sociales: any[];
  selectedOS = -1
  convenios: any[];
  fecha: Date;
  message: string = null;
  
  constructor(
    private osSrv: ObraSocialService,
    private userSrv: UserService,
    private spinner: NgxSpinnerService,
    private convSrv: ConveniosService
  ) { }

  ngOnInit() {
    this.osSrv.getOSGeren().subscribe( result => {
      this.obras_sociales = result;
      console.log(result);
    });
  }

  buscar(){
    var geren = this.obras_sociales.filter(e => e.cod_obra == this.selectedOS)[0].cod_geren;
    this.convSrv.getConvenios(geren, this.selectedOS).subscribe(result => {
      this.convenios = result;
      this.fecha = result[0].fecha;
    })
  }

  print(){
    window.print();
  }

}
