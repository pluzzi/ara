import { TestBed } from '@angular/core/testing';

import { TipoDocumentosService } from './tipo-documentos.service';

describe('TipoDocumentosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipoDocumentosService = TestBed.get(TipoDocumentosService);
    expect(service).toBeTruthy();
  });
});
