import { OrdenPago } from "./orden-pago";

export interface Banco {
    banco: string;
    tipo: string;
    cuenta: string;
    monto?: number;
}
