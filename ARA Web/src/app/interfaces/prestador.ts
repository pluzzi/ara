export interface Prestador {
    cod_pres: number;
    prestador: string;
    sigla: string;
}
