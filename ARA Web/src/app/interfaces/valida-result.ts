export interface ValidaResult {
  matricula: number;
  nombre?: string;
  dias_Pswd?: number;
  token?: string;
  indice: string;
  tipo?: string;
}
