export interface ChangeRequest {
  matricula: number;
  password: string;
  newPassword: string;
  tipoContador: string;
}
