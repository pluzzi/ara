export interface InvoiceDetailItem {
    nro_orden: number;
    cod_nom: string;
    descripcion: string;
    via: number;
    complejidad: number;
    porcentaje: number;
    valor: number;
    recargo: number;
    importe: number;

}
