import { Banco } from "./banco";

export interface OrdenPago {
    tipo: string;
    letra: string;
    cod: string;
    nro: string;
    fecha: Date;
    bancos: Banco[];
}
