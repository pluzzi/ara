export interface Accreditation {
    tipo_op: string;
    letra_op: string;
    cod_op: string;
    nro_op: string;
    fecha_op?: Date;
    grupo?: number;
    nombre_grupo: string;
    importe: number;
    iva: number;
    imp_ret_ig: number;
    imp_ret_ib: number;
    gtos_varios: number;
    imp_gtos_admin: number;
    neto: number;
    tipo_liq: string;
    matricula?: number;
    nombre_op: string;
    nombre_liq: string;
    nombre_ret: string;
}