export interface BankAccreditation {
    tipo_op: string;
    letra_op: string;
    cod_op: string;
    nro_op: string;
    importe_data: number;
    banco: string;
    tipo_cta: string;
    nro_cuenta: string;
    nro_cheque: string;
    fecha_pago: Date;
}
