import { ImageOrden } from "./image-orden";
import { InvoiceDetailItem } from "./invoice-detail-item";

export interface InvoiceDetail {
    nro_orden: number;
    matricula: number;
    nombre: string;
    fecha_practica: Date;
    paciente: string;
    cod_afiliado: string;
    prestador: string;
    tipo_rec: number;
    por_rec: number;
    imagenes: ImageOrden[];
    items: InvoiceDetailItem[];
    seguimiento: string;
}
