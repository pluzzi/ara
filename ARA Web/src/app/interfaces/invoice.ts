export interface Invoice {
  tipo: string;
  letra: string;
  cod: string;
  nro: string;
  fecha_fact: Date;
  neto: number;
  nombre_archivo: string;
  cod_obra: number;
  descripcion: string;
}
