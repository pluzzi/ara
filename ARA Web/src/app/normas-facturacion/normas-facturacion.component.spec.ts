import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NormasFacturacionComponent } from './normas-facturacion.component';

describe('NormasFacturacionComponent', () => {
  let component: NormasFacturacionComponent;
  let fixture: ComponentFixture<NormasFacturacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NormasFacturacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NormasFacturacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
