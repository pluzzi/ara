import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtrarOP'
})
export class FiltrarOPPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return value.filter(item => item.nro_orden === args[0]);
  }

}
