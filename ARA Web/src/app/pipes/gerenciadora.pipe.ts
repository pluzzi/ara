import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gerenciadora'
})
export class GerenciadoraPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if(value===null){
      return ""
    }else{
      return " / " + value
    }
  }

}
