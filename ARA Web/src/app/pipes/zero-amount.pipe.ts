import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'zeroAmount'
})
export class ZeroAmountPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if( value === 0 ){
      return null;
    }else{
      return value;
    }
  }

}
