import { UserService } from './../services/user.service';
import { Router } from '@angular/router';
import { Component } from '@angular/core';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  isExpanded = false;

  constructor(
    private router: Router,
    private userSrv: UserService
  ) { }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  isTipoOQ(): boolean {
    const user = this.userSrv.getUser();
    if(user.tipo !== null){
      return user.tipo.toLowerCase() === 'o' || user.tipo.toLowerCase() === 'q';
    }else{
      return false;
    }
  }

  isTipoMGS(): boolean {
    const user = this.userSrv.getUser();
    if(user.tipo !== null){
      return user.tipo.toLowerCase() === 'm' || user.tipo.toLowerCase() === 'g' || user.tipo.toLowerCase() === 's';
    }else{
      return false;
    }
  }

  isTipoMGSC(): boolean {
    const user = this.userSrv.getUser();
    if(user.tipo !== null){
      return user.tipo.toLowerCase() === 'm' || user.tipo.toLowerCase() === 'g' || user.tipo.toLowerCase() === 's' || user.tipo.toLowerCase() === 'c';
    }else{
      return false;
    }
  }

  isTipoMG(): boolean {
    const user = this.userSrv.getUser();
    if(user.tipo !== null){
      return user.tipo.toLowerCase() === 'm' || user.tipo.toLowerCase() === 'g';
    }else{
      return false;
    }
  }

}
