﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class OrdenesWebDetab
    {
        public int IdOrden { get; set; }
        public byte Item { get; set; }
        public string CodNom { get; set; }
        public byte? Via { get; set; }
    }
}
