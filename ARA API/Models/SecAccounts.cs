﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class SecAccounts
    {
        public int Id { get; set; }
        public int Matricula { get; set; }
        public string Token { get; set; }
        public DateTime UltimoAcceso { get; set; }
    }
}
