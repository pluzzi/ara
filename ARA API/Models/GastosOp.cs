﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class GastosOp
    {
        public int Lote { get; set; }
        public string TipoOp { get; set; }
        public string LetraOp { get; set; }
        public string CodOp { get; set; }
        public string NroOp { get; set; }
        public string Detalle { get; set; }
        public double? Importe { get; set; }
    }
}
