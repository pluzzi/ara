﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class Prestador
    {
        public int cod_pres { get; set; }
        public string prestador { get; set; }
        public string sigla { get; set; }
    }
}
