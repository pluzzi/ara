﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class ConveniosComple
    {
        public int CodObra { get; set; }
        public int CodGeren { get; set; }
        public short Rev { get; set; }
        public string Complejidad { get; set; }
        public double? Importe { get; set; }
        public double? ImporteAnterior { get; set; }
    }
}
