﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class Grupos
    {
        public int Grupo { get; set; }
        public string NombreGrupo { get; set; }
    }
}
