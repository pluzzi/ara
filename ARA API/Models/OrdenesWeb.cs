﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class OrdenesWeb
    {
        public int IdOrden { get; set; }
        public byte Estado { get; set; }
        public DateTime FechaPractica { get; set; }
        public int Matricula { get; set; }
        public int? Grupo { get; set; }
        public int? CodPrest { get; set; }
        public string Cirujano { get; set; }
        public string Paciente { get; set; }
        public int? CodObra { get; set; }
        public string CodAfiliado { get; set; }
        public byte? Urgencia { get; set; }
        public byte? Edad { get; set; }
        public byte? FinSemana { get; set; }
        public DateTime? FechaCarga { get; set; }
        public DateTime? FechaAcepta { get; set; }
        public DateTime? FechaRechazo { get; set; }
        public string MotivoRechazo { get; set; }
        public int? NroOrdenAra { get; set; }
        public int? LoginCarga { get; set; }
        public string Notas { get; set; }
        public int? NroOrdenDes { get; set; }
    }
}
