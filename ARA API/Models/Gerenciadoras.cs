﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class Gerenciadoras
    {
        public int CodGeren { get; set; }
        public string Nombre { get; set; }
        public byte Estado { get; set; }
    }
}
