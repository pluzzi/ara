﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class OSGeren
    {
        public int cod_obra { get; set; }
        public int cod_geren { get; set; }
        public string descripcion { get; set; }
        public string nombre { get; set; }
    }
}
