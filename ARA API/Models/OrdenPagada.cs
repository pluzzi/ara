﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class OrdenPagada
    {
        public int? lote { get; set; }
        public int? nro_orden { get; set; }
        public int? item { get; set; }
        public DateTime? fecha_practica { get; set; }
        public DateTime? fecha_op { get; set; }
        public string? paciente { get; set; }
        public string? sigla { get; set; }
        public string? descripcion { get; set; }
        public Double? importe_fact { get; set; }
        public Double? iva_fact_1 { get; set; }
        public Double? debito_total { get; set; }
        public Double? credito_total { get; set; }
        public Double? neto_total { get; set; }
        public string? nombre_grupo { get; set; }
        public Double? importe { get; set; }
        public Double? porcentaje { get; set; }
        public int? grupo { get; set; }
        public string? tipo_liq { get; set; }
        public string? letra_liq { get; set; }
        public string? cod_liq { get; set; }
        public string? nro_liq { get; set; }
        public byte? orden { get; set; }
        public int? matricula { get; set; }
        public string? nombre { get; set; }
        public int? recargo_edad { get; set; }
        public int? recargo_urg { get; set; }
        public int? recargo_neonato { get; set; }

    }
}
