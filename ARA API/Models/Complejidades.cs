﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class Complejidades
    {
        public string Codigo { get; set; }
        public string Categoria { get; set; }
        public string SubCategoria { get; set; }
        public string Descripcion { get; set; }
        public string Valor { get; set; }
        public DateTime? UltimaActualizacion { get; set; }
        public int Id { get; set; }
        public bool Deleted { get; set; }
    }
}
