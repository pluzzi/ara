﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class Prestadores
    {
        public int CodPres { get; set; }
        public string Prestador { get; set; }
        public string Sigla { get; set; }
        public byte? Estado { get; set; }
    }
}
