﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class LoginsLog
    {
        public DateTime Fecha { get; set; }
        public int Matricula { get; set; }
        public DateTime? FechaC { get; set; }
    }
}
