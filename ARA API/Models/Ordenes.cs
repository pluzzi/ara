﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class Ordenes
    {
        public byte Lote { get; set; }
        public int NroOrden { get; set; }
        public byte Estado { get; set; }
        public int? Matricula { get; set; }
        public int? Grupo { get; set; }
        public DateTime? FechaCarga { get; set; }
        public DateTime? FechaPractica { get; set; }
        public int? CodObra { get; set; }
        public int? CodGeren { get; set; }
        public short? Rev { get; set; }
        public short? CodConv { get; set; }
        public string CodAfiliado { get; set; }
        public string Paciente { get; set; }
        public int? CodPrest { get; set; }
        public string Cirujano { get; set; }
        public decimal? RecargoUrg { get; set; }
        public decimal? RecargoEdad { get; set; }
        public decimal? RecargoFin { get; set; }
        public decimal? RecargoMicro { get; set; }
        public decimal? RecargoNeonato { get; set; }
        public decimal? ImporteTotal { get; set; }
        public decimal? PorBonificaFact { get; set; }
        public decimal? BonificaFact { get; set; }
        public decimal? PorIvaFact1 { get; set; }
        public decimal? IvaFact1 { get; set; }
        public decimal? PorIvaFact2 { get; set; }
        public decimal? IvaFact2 { get; set; }
        public decimal? ImporteFact { get; set; }
        public decimal? DebitoTotal { get; set; }
        public decimal? CreditoTotal { get; set; }
        public decimal? NetoTotal { get; set; }
        public decimal? DebitoTotalAt { get; set; }
        public decimal? CreditoTotalAt { get; set; }
        public decimal? NetoLiquidado { get; set; }
        public byte? TipoFacturacion { get; set; }
        public DateTime? Fecha { get; set; }
        public byte? Refactura { get; set; }
        public byte? LoteOri { get; set; }
        public int? NroOrdenOri { get; set; }
        public int? AuditaNro { get; set; }
        public byte? ModiAudi { get; set; }
        public string Tipo { get; set; }
        public string Letra { get; set; }
        public string Cod { get; set; }
        public string Nro { get; set; }
        public int? MatriculaFc { get; set; }
        public int? GrupoFc { get; set; }
        public string MotivoAudi { get; set; }
        public int? NroOrdenAne { get; set; }
        public string ObsRecep { get; set; }
        public string ObsAdmin { get; set; }
        public string MotivoRechazo { get; set; }
        public int? ClienteAne { get; set; }
        public decimal? PorIvaRi { get; set; }
        public string Seguimiento { get; set; }
        public DateTime? FeRetiro { get; set; }
        public byte? OldNew { get; set; }
        public byte? LoteDes { get; set; }
        public int? NroOrdenDes { get; set; }
        public double? Ajuste { get; set; }
    }
}
