﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class LiquidaDeta
    {
        public int LoteWeb { get; set; }
        public string Tipo { get; set; }
        public string Letra { get; set; }
        public string Cod { get; set; }
        public string Nro { get; set; }
        public short ItemLiq { get; set; }
        public byte? Lote { get; set; }
        public int? NroOrden { get; set; }
        public byte? ItemOrd { get; set; }
        public double? Importe { get; set; }
        public double? ImpIva { get; set; }
        public int? MatriculaFc { get; set; }
        public int? GrupoFc { get; set; }
        public string Detalle { get; set; }
    }
}
