﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace ARA_API.Models
{
    public partial class Context : DbContext
    {
        public Context()
        {
        }

        public Context(DbContextOptions<Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Anestesiologos> Anestesiologos { get; set; }
        public virtual DbSet<ArchivosCab> ArchivosCab { get; set; }
        public virtual DbSet<ArchivosDet> ArchivosDet { get; set; }
        public virtual DbSet<Complejidades> Complejidades { get; set; }
        public virtual DbSet<ComplejidadesCategorias> ComplejidadesCategorias { get; set; }
        public virtual DbSet<ComplejidadesSubcategorias> ComplejidadesSubcategorias { get; set; }
        public virtual DbSet<Convenios> Convenios { get; set; }
        public virtual DbSet<ConveniosComple> ConveniosComple { get; set; }
        public virtual DbSet<DataDetalle> DataDetalle { get; set; }
        public virtual DbSet<GastosOp> GastosOp { get; set; }
        public virtual DbSet<Gerenciadoras> Gerenciadoras { get; set; }
        public virtual DbSet<Grupos> Grupos { get; set; }
        public virtual DbSet<GruposAnest> GruposAnest { get; set; }
        public virtual DbSet<LiquidaDeta> LiquidaDeta { get; set; }
        public virtual DbSet<LiquidaGrupos> LiquidaGrupos { get; set; }
        public virtual DbSet<Logins> Logins { get; set; }
        public virtual DbSet<LoginsLog> LoginsLog { get; set; }
        public virtual DbSet<Nomenclador> Nomenclador { get; set; }
        public virtual DbSet<ObrasSociales> ObrasSociales { get; set; }
        public virtual DbSet<OrdenPago> OrdenPago { get; set; }
        public virtual DbSet<Ordenes> Ordenes { get; set; }
        public virtual DbSet<OrdenesDeta> OrdenesDeta { get; set; }
        public virtual DbSet<OrdenesDetaDeb> OrdenesDetaDeb { get; set; }
        public virtual DbSet<OrdenesWeb> OrdenesWeb { get; set; }
        public virtual DbSet<OrdenesWebDeta> OrdenesWebDeta { get; set; }
        public virtual DbSet<OrdenesWebDetab> OrdenesWebDetab { get; set; }
        public virtual DbSet<OrdenesWebImagen> OrdenesWebImagen { get; set; }
        public virtual DbSet<Prestadores> Prestadores { get; set; }
        public virtual DbSet<SecAccounts> SecAccounts { get; set; }
        public virtual DbSet<IntentosResult> IntetosResult { get; set; }
        public virtual DbSet<ValidaResult> ValidaResult { get; set; }
        public virtual DbSet<ChangeRequest> ChangeRequest { get; set; }
        public virtual DbSet<BoolResult> BoolResult { get; set; }
        public virtual DbSet<Invoice> Invoice { get; set; }
        public virtual DbSet<Images> Images { get; set; }
        public virtual DbSet<StringResult> StringResult { get; set; }
        public virtual DbSet<InvoiceDetail> InvoiceDetail { get; set; }
        public virtual DbSet<Accreditation> Accreditation { get; set; }
        public virtual DbSet<BankAccreditation> BankAccreditation { get; set; }
        public virtual DbSet<ImagenOrden> ImagenOrden { get; set; }
        public virtual DbSet<InvoiceDetailItem> InvoiceDetailItem { get; set; }
        public virtual DbSet<Gastos> Gastos { get; set; }
        public virtual DbSet<ObraSocial> ObraSocial { get; set; }
        public virtual DbSet<OrdenPagada> OrdenPagada { get; set; }
        public virtual DbSet<Prestador> Prestador { get; set; }
        public virtual DbSet<OS> OS { get; set; }
        public virtual DbSet<TipoDocumento> TipoDocumento { get; set; }
        public virtual DbSet<ArchivoDownload> ArchivoDownload { get; set; }
        public virtual DbSet<OSGeren> OSGeren { get; set; }

        public virtual DbSet<Convenio> Convenio { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var builder = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                    
                IConfiguration configuration = builder.Build();


                optionsBuilder.UseSqlServer(configuration.GetConnectionString("ARADatabaseProd"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Anestesiologos>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("anestesiologos");

                entity.Property(e => e.Matricula).HasColumnName("matricula");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo)
                    .HasColumnName("tipo")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<ArchivosCab>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("archivos_cab");

                entity.Property(e => e.Codigo).HasColumnName("codigo");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TipoMuestra).HasColumnName("tipo_muestra");
            });

            modelBuilder.Entity<ArchivosDet>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("archivos_det");

                entity.Property(e => e.Codigo).HasColumnName("codigo");

                entity.Property(e => e.Tipo)
                    .HasColumnName("tipo")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<Complejidades>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("complejidades");

                entity.Property(e => e.Categoria)
                    .IsRequired()
                    .HasColumnName("categoria")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Codigo)
                    .IsRequired()
                    .HasColumnName("codigo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Deleted).HasColumnName("deleted");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("descripcion")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.SubCategoria)
                    .IsRequired()
                    .HasColumnName("subCategoria")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UltimaActualizacion)
                    .HasColumnName("ultimaActualizacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.Valor)
                    .IsRequired()
                    .HasColumnName("valor")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ComplejidadesCategorias>(entity =>
            {
                

                entity.ToTable("complejidades_categorias");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("descripcion")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Id).HasColumnName("id");
            });

            modelBuilder.Entity<ComplejidadesSubcategorias>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("complejidades_subcategorias");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("descripcion")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Id).HasColumnName("id");
            });

            modelBuilder.Entity<Convenios>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("convenios");

                entity.Property(e => e.CodGeren).HasColumnName("cod_geren");

                entity.Property(e => e.CodObra).HasColumnName("cod_obra");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.FechaAnterior)
                    .HasColumnName("fecha_anterior")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaDesde)
                    .HasColumnName("fecha_desde")
                    .HasColumnType("datetime");

                entity.Property(e => e.Guia)
                    .HasColumnName("guia")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.GuiaAnterior)
                    .HasColumnName("guia_anterior")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Rev).HasColumnName("rev");
            });

            modelBuilder.Entity<ConveniosComple>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("convenios_comple");

                entity.Property(e => e.CodGeren).HasColumnName("cod_geren");

                entity.Property(e => e.CodObra).HasColumnName("cod_obra");

                entity.Property(e => e.Complejidad)
                    .HasColumnName("complejidad")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Importe).HasColumnName("importe");

                entity.Property(e => e.ImporteAnterior).HasColumnName("importe_anterior");

                entity.Property(e => e.Rev).HasColumnName("rev");
            });

            modelBuilder.Entity<DataDetalle>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("data_detalle");

                entity.Property(e => e.Banco)
                    .HasColumnName("banco")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodOp)
                    .IsRequired()
                    .HasColumnName("cod_op")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FechaPago)
                    .HasColumnName("fecha_pago")
                    .HasColumnType("datetime");

                entity.Property(e => e.ImporteData).HasColumnName("importe_data");

                entity.Property(e => e.LetraOp)
                    .IsRequired()
                    .HasColumnName("letra_op")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Lote).HasColumnName("lote");

                entity.Property(e => e.NroCheque)
                    .HasColumnName("nro_cheque")
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.NroCuenta)
                    .HasColumnName("nro_cuenta")
                    .HasMaxLength(17)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.NroOp)
                    .IsRequired()
                    .HasColumnName("nro_op")
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TipoCta)
                    .HasColumnName("tipo_cta")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TipoOp)
                    .IsRequired()
                    .HasColumnName("tipo_op")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<GastosOp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("gastos_op");

                entity.Property(e => e.CodOp)
                    .IsRequired()
                    .HasColumnName("cod_op")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Detalle)
                    .HasColumnName("detalle")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Importe).HasColumnName("importe");

                entity.Property(e => e.LetraOp)
                    .IsRequired()
                    .HasColumnName("letra_op")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Lote).HasColumnName("lote");

                entity.Property(e => e.NroOp)
                    .IsRequired()
                    .HasColumnName("nro_op")
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TipoOp)
                    .IsRequired()
                    .HasColumnName("tipo_op")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<Gerenciadoras>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("gerenciadoras");

                entity.Property(e => e.CodGeren).HasColumnName("cod_geren");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Grupos>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("grupos");

                entity.Property(e => e.Grupo).HasColumnName("grupo");

                entity.Property(e => e.NombreGrupo)
                    .HasColumnName("nombre_grupo")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GruposAnest>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("grupos_anest");

                entity.Property(e => e.Grupo).HasColumnName("grupo");

                entity.Property(e => e.Guardia)
                    .HasColumnName("guardia")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Matricula).HasColumnName("matricula");

                entity.Property(e => e.NoDta)
                    .HasColumnName("no_dta")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.PagaIva)
                    .HasColumnName("paga_iva")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Porcentaje)
                    .HasColumnName("porcentaje")
                    .HasColumnType("decimal(6, 2)");
            });

            modelBuilder.Entity<LiquidaDeta>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("liquida_deta");

                entity.Property(e => e.Cod)
                    .IsRequired()
                    .HasColumnName("cod")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Detalle)
                    .HasColumnName("detalle")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.GrupoFc).HasColumnName("grupo_fc");

                entity.Property(e => e.ImpIva).HasColumnName("imp_iva");

                entity.Property(e => e.Importe).HasColumnName("importe");

                entity.Property(e => e.ItemLiq).HasColumnName("item_liq");

                entity.Property(e => e.ItemOrd).HasColumnName("item_ord");

                entity.Property(e => e.Letra)
                    .IsRequired()
                    .HasColumnName("letra")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Lote).HasColumnName("lote");

                entity.Property(e => e.LoteWeb).HasColumnName("lote_web");

                entity.Property(e => e.MatriculaFc).HasColumnName("matricula_fc");

                entity.Property(e => e.Nro)
                    .IsRequired()
                    .HasColumnName("nro")
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.NroOrden).HasColumnName("nro_orden");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasColumnName("tipo")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<LiquidaGrupos>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("liquida_grupos");

                entity.Property(e => e.Cod)
                    .IsRequired()
                    .HasColumnName("cod")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodOp)
                    .HasColumnName("cod_op")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodOpIva)
                    .HasColumnName("cod_op_iva")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Grupo).HasColumnName("grupo");

                entity.Property(e => e.ImpIva).HasColumnName("imp_iva");

                entity.Property(e => e.Importe).HasColumnName("importe");

                entity.Property(e => e.Letra)
                    .IsRequired()
                    .HasColumnName("letra")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LetraOp)
                    .HasColumnName("letra_op")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LetraOpIva)
                    .HasColumnName("letra_op_iva")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LoteWeb).HasColumnName("lote_web");

                entity.Property(e => e.Matricula).HasColumnName("matricula");

                entity.Property(e => e.NoGcias).HasColumnName("no_gcias");

                entity.Property(e => e.Nro)
                    .IsRequired()
                    .HasColumnName("nro")
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.NroOp)
                    .HasColumnName("nro_op")
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.NroOpIva)
                    .HasColumnName("nro_op_iva")
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Porcentaje)
                    .HasColumnName("porcentaje")
                    .HasColumnType("decimal(6, 2)");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasColumnName("tipo")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TipoOp)
                    .HasColumnName("tipo_op")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TipoOpIva)
                    .HasColumnName("tipo_op_iva")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<Logins>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("logins");

                entity.Property(e => e.Cambio)
                    .HasColumnName("cambio")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CambioC)
                    .HasColumnName("cambio_c")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FecAct)
                    .HasColumnName("fec_act")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecActC)
                    .HasColumnName("fec_act_c")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecToken)
                    .HasColumnName("fec_token")
                    .HasColumnType("datetime");

                entity.Property(e => e.Intentos).HasColumnName("intentos");

                entity.Property(e => e.IntentosC).HasColumnName("intentos_c");

                entity.Property(e => e.Matricula).HasColumnName("matricula");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordC)
                    .HasColumnName("password_c")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Token)
                    .HasColumnName("token")
                    .HasMaxLength(80)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LoginsLog>(entity =>
            {
                entity.HasKey(e => new { e.Fecha, e.Matricula });

                entity.ToTable("logins_log");

                entity.Property(e => e.Fecha)
                    .HasColumnName("fecha")
                    .HasColumnType("datetime");

                entity.Property(e => e.Matricula).HasColumnName("matricula");

                entity.Property(e => e.FechaC)
                    .HasColumnName("fecha_c")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Nomenclador>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("nomenclador");

                entity.Property(e => e.CodNom)
                    .IsRequired()
                    .HasColumnName("cod_nom")
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Complejidad)
                    .HasColumnName("complejidad")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("descripcion")
                    .HasMaxLength(70)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ObrasSociales>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("obras_sociales");

                entity.Property(e => e.CodObra).HasColumnName("cod_obra");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("descripcion")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OrdenPago>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("orden_pago");

                entity.Property(e => e.CodOp)
                    .IsRequired()
                    .HasColumnName("cod_op")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FechaOp)
                    .HasColumnName("fecha_op")
                    .HasColumnType("datetime");

                entity.Property(e => e.Grupo).HasColumnName("grupo");

                entity.Property(e => e.GtosVarios).HasColumnName("gtos_varios");

                entity.Property(e => e.ImpGtosAdmin).HasColumnName("imp_gtos_admin");

                entity.Property(e => e.ImpRetIb).HasColumnName("imp_ret_ib");

                entity.Property(e => e.ImpRetIg).HasColumnName("imp_ret_ig");

                entity.Property(e => e.Importe).HasColumnName("importe");

                entity.Property(e => e.Iva).HasColumnName("iva");

                entity.Property(e => e.LetraOp)
                    .IsRequired()
                    .HasColumnName("letra_op")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Lote).HasColumnName("lote");

                entity.Property(e => e.Matricula).HasColumnName("matricula");

                entity.Property(e => e.Neto).HasColumnName("neto");

                entity.Property(e => e.NroOp)
                    .IsRequired()
                    .HasColumnName("nro_op")
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TipoLiq)
                    .HasColumnName("tipo_liq")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TipoOp)
                    .IsRequired()
                    .HasColumnName("tipo_op")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<Ordenes>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ordenes");

                entity.Property(e => e.Ajuste).HasColumnName("ajuste");

                entity.Property(e => e.AuditaNro).HasColumnName("audita_nro");

                entity.Property(e => e.BonificaFact)
                    .HasColumnName("bonifica_fact")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Cirujano)
                    .HasColumnName("cirujano")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ClienteAne).HasColumnName("cliente_ane");

                entity.Property(e => e.Cod)
                    .HasColumnName("cod")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodAfiliado)
                    .HasColumnName("cod_afiliado")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CodConv).HasColumnName("cod_conv");

                entity.Property(e => e.CodGeren).HasColumnName("cod_geren");

                entity.Property(e => e.CodObra).HasColumnName("cod_obra");

                entity.Property(e => e.CodPrest).HasColumnName("cod_prest");

                entity.Property(e => e.CreditoTotal)
                    .HasColumnName("credito_total")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CreditoTotalAt)
                    .HasColumnName("credito_total_at")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DebitoTotal)
                    .HasColumnName("debito_total")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DebitoTotalAt)
                    .HasColumnName("debito_total_at")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.FeRetiro)
                    .HasColumnName("fe_retiro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fecha)
                    .HasColumnName("fecha")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaCarga)
                    .HasColumnName("fecha_carga")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaPractica)
                    .HasColumnName("fecha_practica")
                    .HasColumnType("datetime");

                entity.Property(e => e.Grupo).HasColumnName("grupo");

                entity.Property(e => e.GrupoFc).HasColumnName("grupo_fc");

                entity.Property(e => e.ImporteFact)
                    .HasColumnName("importe_fact")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ImporteTotal)
                    .HasColumnName("importe_total")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.IvaFact1)
                    .HasColumnName("iva_fact_1")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.IvaFact2)
                    .HasColumnName("iva_fact_2")
                    .HasColumnType("decimal(6, 2)");

                entity.Property(e => e.Letra)
                    .HasColumnName("letra")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Lote).HasColumnName("lote");

                entity.Property(e => e.LoteDes).HasColumnName("lote_des");

                entity.Property(e => e.LoteOri).HasColumnName("lote_ori");

                entity.Property(e => e.Matricula).HasColumnName("matricula");

                entity.Property(e => e.MatriculaFc).HasColumnName("matricula_fc");

                entity.Property(e => e.ModiAudi).HasColumnName("modi_audi");

                entity.Property(e => e.MotivoAudi)
                    .HasColumnName("motivo_audi")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MotivoRechazo)
                    .HasColumnName("motivo_rechazo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NetoLiquidado)
                    .HasColumnName("neto_liquidado")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.NetoTotal)
                    .HasColumnName("neto_total")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Nro)
                    .HasColumnName("nro")
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.NroOrden).HasColumnName("nro_orden");

                entity.Property(e => e.NroOrdenAne).HasColumnName("nro_orden_ane");

                entity.Property(e => e.NroOrdenDes).HasColumnName("nro_orden_des");

                entity.Property(e => e.NroOrdenOri).HasColumnName("nro_orden_ori");

                entity.Property(e => e.ObsAdmin)
                    .HasColumnName("obs_admin")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ObsRecep)
                    .HasColumnName("obs_recep")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.OldNew).HasColumnName("old_new");

                entity.Property(e => e.Paciente)
                    .HasColumnName("paciente")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.PorBonificaFact)
                    .HasColumnName("por_bonifica_fact")
                    .HasColumnType("decimal(6, 2)");

                entity.Property(e => e.PorIvaFact1)
                    .HasColumnName("por_iva_fact_1")
                    .HasColumnType("decimal(6, 2)");

                entity.Property(e => e.PorIvaFact2)
                    .HasColumnName("por_iva_fact_2")
                    .HasColumnType("decimal(6, 2)");

                entity.Property(e => e.PorIvaRi)
                    .HasColumnName("por_iva_ri")
                    .HasColumnType("decimal(6, 2)");

                entity.Property(e => e.RecargoEdad)
                    .HasColumnName("recargo_edad")
                    .HasColumnType("decimal(8, 2)");

                entity.Property(e => e.RecargoFin)
                    .HasColumnName("recargo_fin")
                    .HasColumnType("decimal(8, 2)");

                entity.Property(e => e.RecargoMicro)
                    .HasColumnName("recargo_micro")
                    .HasColumnType("decimal(8, 2)");

                entity.Property(e => e.RecargoNeonato)
                    .HasColumnName("recargo_neonato")
                    .HasColumnType("decimal(8, 2)");

                entity.Property(e => e.RecargoUrg)
                    .HasColumnName("recargo_urg")
                    .HasColumnType("decimal(8, 2)");

                entity.Property(e => e.Refactura).HasColumnName("refactura");

                entity.Property(e => e.Rev).HasColumnName("rev");

                entity.Property(e => e.Seguimiento)
                    .HasColumnName("seguimiento")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo)
                    .HasColumnName("tipo")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TipoFacturacion).HasColumnName("tipo_facturacion");
            });

            modelBuilder.Entity<OrdenesDeta>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ordenes_deta");

                entity.Property(e => e.Ajuste).HasColumnName("ajuste");

                entity.Property(e => e.BonificaFact)
                    .HasColumnName("bonifica_fact")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CodNom)
                    .HasColumnName("cod_nom")
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Complejidad)
                    .HasColumnName("complejidad")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Credito)
                    .HasColumnName("credito")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CreditoAt)
                    .HasColumnName("credito_at")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Debito)
                    .HasColumnName("debito")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DebitoAt)
                    .HasColumnName("debito_at")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("descripcion")
                    .HasMaxLength(70)
                    .IsUnicode(false);

                entity.Property(e => e.Importe)
                    .HasColumnName("importe")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ImporteFact)
                    .HasColumnName("importe_fact")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ItemOrd).HasColumnName("item_ord");

                entity.Property(e => e.IvaCred)
                    .HasColumnName("iva_cred")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.IvaDeb)
                    .HasColumnName("iva_deb")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.IvaFact1)
                    .HasColumnName("iva_fact_1")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.IvaFact2)
                    .HasColumnName("iva_fact_2")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Lote).HasColumnName("lote");

                entity.Property(e => e.NetoLiquida)
                    .HasColumnName("neto_liquida")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.NetoTotal)
                    .HasColumnName("neto_total")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.NroOrden).HasColumnName("nro_orden");

                entity.Property(e => e.Porcentaje)
                    .HasColumnName("porcentaje")
                    .HasColumnType("decimal(8, 2)");

                entity.Property(e => e.Recargo)
                    .HasColumnName("recargo")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Valor)
                    .HasColumnName("valor")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ValorFijo).HasColumnName("valor_fijo");

                entity.Property(e => e.Via).HasColumnName("via");
            });

            modelBuilder.Entity<OrdenesDetaDeb>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ordenes_deta_deb");

                entity.Property(e => e.CodLiq)
                    .HasColumnName("cod_liq")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodObra).HasColumnName("cod_obra");

                entity.Property(e => e.CodOpIva)
                    .HasColumnName("cod_op_iva")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Credito)
                    .HasColumnName("credito")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Debito)
                    .HasColumnName("debito")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("descripcion")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.EstadoIva).HasColumnName("estado_iva");

                entity.Property(e => e.ItemDeb).HasColumnName("item_deb");

                entity.Property(e => e.ItemOrd).HasColumnName("item_ord");

                entity.Property(e => e.IvaCredito)
                    .HasColumnName("iva_credito")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.IvaDebito)
                    .HasColumnName("iva_debito")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.LetraLiq)
                    .HasColumnName("letra_liq")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LetraOpIva)
                    .HasColumnName("letra_op_iva")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Lote).HasColumnName("lote");

                entity.Property(e => e.LoteWeb).HasColumnName("lote_web");

                entity.Property(e => e.NroLiq)
                    .HasColumnName("nro_liq")
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.NroOpIva)
                    .HasColumnName("nro_op_iva")
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.NroOrden).HasColumnName("nro_orden");

                entity.Property(e => e.Tipo).HasColumnName("tipo");

                entity.Property(e => e.TipoDto).HasColumnName("tipo_dto");

                entity.Property(e => e.TipoLiq)
                    .HasColumnName("tipo_liq")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TipoOpIva)
                    .HasColumnName("tipo_op_iva")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<OrdenesWeb>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ordenes_web");

                entity.Property(e => e.Cirujano)
                    .HasColumnName("cirujano")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CodAfiliado)
                    .HasColumnName("cod_afiliado")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CodObra).HasColumnName("cod_obra");

                entity.Property(e => e.CodPrest).HasColumnName("cod_prest");

                entity.Property(e => e.Edad).HasColumnName("edad");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.FechaAcepta)
                    .HasColumnName("fecha_acepta")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaCarga)
                    .HasColumnName("fecha_carga")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaPractica)
                    .HasColumnName("fecha_practica")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaRechazo)
                    .HasColumnName("fecha_rechazo")
                    .HasColumnType("datetime");

                entity.Property(e => e.FinSemana).HasColumnName("fin_semana");

                entity.Property(e => e.Grupo).HasColumnName("grupo");

                entity.Property(e => e.IdOrden).HasColumnName("id_orden");

                entity.Property(e => e.LoginCarga).HasColumnName("login_carga");

                entity.Property(e => e.Matricula).HasColumnName("matricula");

                entity.Property(e => e.MotivoRechazo)
                    .HasColumnName("motivo_rechazo")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Notas)
                    .HasColumnName("notas")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.NroOrdenAra).HasColumnName("nro_orden_ara");

                entity.Property(e => e.NroOrdenDes).HasColumnName("nro_orden_des");

                entity.Property(e => e.Paciente)
                    .HasColumnName("paciente")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.Urgencia).HasColumnName("urgencia");
            });

            modelBuilder.Entity<OrdenesWebDeta>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ordenes_web_deta");

                entity.Property(e => e.CodNom)
                    .IsRequired()
                    .HasColumnName("cod_nom")
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.IdOrden).HasColumnName("id_orden");

                entity.Property(e => e.Item).HasColumnName("item");

                entity.Property(e => e.Via).HasColumnName("via");
            });

            modelBuilder.Entity<OrdenesWebDetab>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ordenes_web_detab");

                entity.Property(e => e.CodNom)
                    .IsRequired()
                    .HasColumnName("cod_nom")
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.IdOrden).HasColumnName("id_orden");

                entity.Property(e => e.Item).HasColumnName("item");

                entity.Property(e => e.Via).HasColumnName("via");
            });

            modelBuilder.Entity<OrdenesWebImagen>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ordenes_web_imagen");

                entity.Property(e => e.IdOrden).HasColumnName("id_orden");

                entity.Property(e => e.Item).HasColumnName("item");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Prestadores>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("prestadores");

                entity.Property(e => e.CodPres).HasColumnName("cod_pres");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.Prestador)
                    .IsRequired()
                    .HasColumnName("prestador")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.Sigla)
                    .HasColumnName("sigla")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SecAccounts>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("sec_accounts");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Matricula).HasColumnName("matricula");

                entity.Property(e => e.Token)
                    .IsRequired()
                    .HasColumnName("token")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UltimoAcceso)
                    .HasColumnName("ultimoAcceso")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<ValidaResult>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<IntentosResult>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<BoolResult>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<ChangeRequest>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<Invoice>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<Images>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<StringResult>(entity =>
            {
                entity.HasNoKey();
            });
            
            modelBuilder.Entity<InvoiceDetail>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<Accreditation>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<BankAccreditation>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<ImagenOrden>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<InvoiceDetailItem>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<Gastos>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<ObraSocial>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<OrdenPagada>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<Prestador>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<OS>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<TipoDocumento>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<ArchivoDownload>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<OSGeren>(entity =>
            {
                entity.HasNoKey();
            });
            modelBuilder.Entity<Convenio>(entity =>
            {
                entity.HasNoKey();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
