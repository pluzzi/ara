﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class ArchivosDet
    {
        public int Codigo { get; set; }
        public string Tipo { get; set; }
    }
}
