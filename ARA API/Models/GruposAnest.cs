﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class GruposAnest
    {
        public int Grupo { get; set; }
        public int Matricula { get; set; }
        public decimal Porcentaje { get; set; }
        public string Guardia { get; set; }
        public string PagaIva { get; set; }
        public string NoDta { get; set; }
    }
}
