﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class Gastos
    {
        public string detalle { get; set; }
        public Double importe { get; set; }
    }
}
