﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class Convenios
    {
        public int CodObra { get; set; }
        public int CodGeren { get; set; }
        public short Rev { get; set; }
        public byte? Estado { get; set; }
        public DateTime? FechaDesde { get; set; }
        public DateTime? FechaAnterior { get; set; }
        public string Guia { get; set; }
        public string GuiaAnterior { get; set; }
    }
}
