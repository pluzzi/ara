﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class OrdenesWebImagen
    {
        public int IdOrden { get; set; }
        public byte Item { get; set; }
        public string Url { get; set; }
    }
}
