﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class OrdenesDetaDeb
    {
        public int LoteWeb { get; set; }
        public byte Lote { get; set; }
        public int NroOrden { get; set; }
        public byte ItemOrd { get; set; }
        public short ItemDeb { get; set; }
        public byte Tipo { get; set; }
        public string Descripcion { get; set; }
        public decimal? Debito { get; set; }
        public decimal? Credito { get; set; }
        public byte? Estado { get; set; }
        public string TipoLiq { get; set; }
        public string LetraLiq { get; set; }
        public string CodLiq { get; set; }
        public string NroLiq { get; set; }
        public decimal? IvaDebito { get; set; }
        public decimal? IvaCredito { get; set; }
        public byte? TipoDto { get; set; }
        public byte? EstadoIva { get; set; }
        public string TipoOpIva { get; set; }
        public string LetraOpIva { get; set; }
        public string CodOpIva { get; set; }
        public string NroOpIva { get; set; }
        public int? CodObra { get; set; }
    }
}
