﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class BankAccreditation
    {
        public string tipo_op { get; set; }
        public string letra_op { get; set; }
        public string cod_op { get; set; }
        public string nro_op { get; set; }
        public Double importe_data { get; set; }
        public string banco { get; set; }
        public string tipo_cta { get; set; }
        public string nro_cuenta { get; set; }
        public string nro_cheque { get; set; }
        public DateTime fecha_pago { get; set; }


    }
}
