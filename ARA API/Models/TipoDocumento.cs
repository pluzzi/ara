﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class TipoDocumento
    {
        public int codigo { get; set; }
        public string nombre { get; set; }
        public byte tipo_muestra { get; set; }
    }
}
