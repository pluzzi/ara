﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class ObraSocial
    {
        public int cod_obra { get; set; }
        public string nombre { get; set; }
    }
}
