﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class ComplejidadesCategorias
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
