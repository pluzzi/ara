﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class ArchivoDownload
    {
        public int codigo { get; set; }
        public string tipo { get; set; }
        public string nombre { get; set; }

    }
}
