﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class ObrasSociales
    {
        public int CodObra { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public byte Estado { get; set; }
    }
}
