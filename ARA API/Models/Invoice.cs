﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class Invoice
    {
        public string tipo { get; set; }
        public string letra { get; set; }
        public string cod { get; set; }
        public string nro { get; set; }
        public DateTime fecha_fact { get; set; }

        [Column(TypeName = "decimal(18,4)")]
        public decimal neto { get; set; }
        public string nombre_archivo { get; set; }
        public int cod_obra { get; set; }
        public string descripcion { get; set; }


    }
}
