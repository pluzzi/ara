﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class ValidaResult
    {
        public int Matricula { get; set; }
        public string Nombre { get; set; }
        public int? Dias_Pswd { get; set; }
        public string? Token { get; set; }
        public string Indice { get; set; }
        public string tipo { get; set; }
    }
}
