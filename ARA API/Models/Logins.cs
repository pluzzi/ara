﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class Logins
    {
        public int Matricula { get; set; }
        public string Password { get; set; }
        public DateTime? FecAct { get; set; }
        public int? Intentos { get; set; }
        public string Cambio { get; set; }
        public string PasswordC { get; set; }
        public DateTime? FecActC { get; set; }
        public int? IntentosC { get; set; }
        public string CambioC { get; set; }
        public string Token { get; set; }
        public DateTime? FecToken { get; set; }
    }
}
