﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class DataDetalle
    {
        public int Lote { get; set; }
        public string TipoOp { get; set; }
        public string LetraOp { get; set; }
        public string CodOp { get; set; }
        public string NroOp { get; set; }
        public DateTime? FechaPago { get; set; }
        public double? ImporteData { get; set; }
        public string TipoCta { get; set; }
        public string Banco { get; set; }
        public string NroCuenta { get; set; }
        public string NroCheque { get; set; }
    }
}
