﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class Anestesiologos
    {
        public int Matricula { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
    }
}
