﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class Convenio
    {
        public int orden { get; set; }
        public string complejidad { get; set; }
        public Double valor_anterior { get; set; }
        public Double valor_actual { get; set; }
        public Double porcentaje { get; set; }
        public Double promedio { get; set; }
        public DateTime fecha { get; set; }
        public DateTime? fec_ant { get; set; }
        public string guia { get; set; }
        public string guia_ant { get; set; }

    }
}
