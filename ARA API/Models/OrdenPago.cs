﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class OrdenPago
    {
        public int Lote { get; set; }
        public string TipoOp { get; set; }
        public string LetraOp { get; set; }
        public string CodOp { get; set; }
        public string NroOp { get; set; }
        public DateTime? FechaOp { get; set; }
        public int? Matricula { get; set; }
        public int? Grupo { get; set; }
        public double? Importe { get; set; }
        public double? Iva { get; set; }
        public double? ImpRetIg { get; set; }
        public double? ImpRetIb { get; set; }
        public double? GtosVarios { get; set; }
        public double? ImpGtosAdmin { get; set; }
        public double? Neto { get; set; }
        public string TipoLiq { get; set; }
    }
}
