﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class LiquidaGrupos
    {
        public int LoteWeb { get; set; }
        public string Tipo { get; set; }
        public string Letra { get; set; }
        public string Cod { get; set; }
        public string Nro { get; set; }
        public int Matricula { get; set; }
        public int Grupo { get; set; }
        public decimal? Porcentaje { get; set; }
        public double? Importe { get; set; }
        public double? ImpIva { get; set; }
        public string TipoOp { get; set; }
        public string LetraOp { get; set; }
        public string CodOp { get; set; }
        public string NroOp { get; set; }
        public string TipoOpIva { get; set; }
        public string LetraOpIva { get; set; }
        public string CodOpIva { get; set; }
        public string NroOpIva { get; set; }
        public byte? NoGcias { get; set; }
    }
}
