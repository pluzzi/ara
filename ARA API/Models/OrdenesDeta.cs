﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class OrdenesDeta
    {
        public byte Lote { get; set; }
        public int NroOrden { get; set; }
        public byte ItemOrd { get; set; }
        public string CodNom { get; set; }
        public string Descripcion { get; set; }
        public byte? Via { get; set; }
        public string Complejidad { get; set; }
        public decimal? Porcentaje { get; set; }
        public decimal? Valor { get; set; }
        public decimal? Recargo { get; set; }
        public decimal? Importe { get; set; }
        public byte? ValorFijo { get; set; }
        public decimal? BonificaFact { get; set; }
        public decimal? IvaFact1 { get; set; }
        public decimal? IvaFact2 { get; set; }
        public decimal? ImporteFact { get; set; }
        public decimal? Debito { get; set; }
        public decimal? IvaDeb { get; set; }
        public decimal? Credito { get; set; }
        public decimal? IvaCred { get; set; }
        public decimal? NetoTotal { get; set; }
        public decimal? DebitoAt { get; set; }
        public decimal? CreditoAt { get; set; }
        public decimal? NetoLiquida { get; set; }
        public double? Ajuste { get; set; }
    }
}
