﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class ArchivosCab
    {
        public int Codigo { get; set; }
        public string Nombre { get; set; }
        public byte? TipoMuestra { get; set; }
    }
}
