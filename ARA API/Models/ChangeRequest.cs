﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class ChangeRequest
    {
        public int Matricula { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string TipoContador { get; set; }
    }
}
