﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class Accreditation
    {
        public string tipo_op { get; set; }
        public string letra_op { get; set; }
        public string cod_op { get; set; }
        public string nro_op { get; set; }
        public DateTime fecha_op { get; set; }
        public int grupo { get; set; }
        public string nombre_grupo { get; set; }
        public Double importe { get; set; }
        public Double iva { get; set; }
        public Double imp_ret_ig { get; set; }
        public Double imp_ret_ib { get; set; }
        public Double gtos_varios { get; set; }
        public Double imp_gtos_admin { get; set; }
        public Double neto { get; set; }
        public string tipo_liq { get; set; }
        public int matricula { get; set; }
        public string nombre_op { get; set; }
        public string nombre_liq { get; set; }
        public string nombre_ret { get; set; }


    }
}
