﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class ImagenOrden
    {
        public string url { get; set; }
        public int nro_orden { get; set; }
        public byte item { get; set; }
    }
}
