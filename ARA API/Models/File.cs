﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class File
    {
        public string path { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
    }
}
