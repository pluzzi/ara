﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class InvoiceDetail
    {
        public int nro_orden { get; set; }
        public int matricula { get; set; }
        public string nombre { get; set; }
        public DateTime fecha_practica { get; set; }
        public string paciente { get; set; }
        public string cod_afiliado { get; set; }
        public string prestador { get; set; }
        public int tipo_rec { get; set; }
        public decimal por_rec { get; set; }

        [NotMapped]
        public List<ImagenOrden> imagenes { get; set; }

        [NotMapped]
        public List<InvoiceDetailItem> items { get; set; }

        public string seguimiento { get; set; }


    }
}
