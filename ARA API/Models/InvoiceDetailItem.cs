﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class InvoiceDetailItem
    {
        public int nro_orden { get; set; }
        public string cod_nom { get; set; }
        public string descripcion { get; set; }
        public byte via { get; set; }
        public string complejidad { get; set; }
        public decimal porcentaje { get; set; }
        public decimal valor { get; set; }
        public decimal recargo { get; set; }
        public decimal importe { get; set; }
    }
}
