﻿using System;
using System.Collections.Generic;

namespace ARA_API.Models
{
    public partial class Nomenclador
    {
        public string CodNom { get; set; }
        public string Descripcion { get; set; }
        public string Complejidad { get; set; }
    }
}
