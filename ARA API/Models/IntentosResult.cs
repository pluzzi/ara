﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ARA_API.Models
{
    public class IntentosResult
    {
        public int? Intentos { get; set; }
        public string Cambio { get; set; }
    }
}
