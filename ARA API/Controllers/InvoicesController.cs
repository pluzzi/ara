﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using ARA_API.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ARA_API.Controllers
{
    [EnableCors("AllowAll")]
    [Route("[controller]")]
    [ApiController]
    public class InvoicesController : ControllerBase
    {
        private Context _context;

        public InvoicesController(Context context)
        {
            _context = context;
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("{matricula}/{desde}/{hasta}/{cod_obra}")]
        public List<Invoice> GetInvoices([FromRoute] int matricula, [FromRoute] DateTime desde, [FromRoute] DateTime hasta, [FromRoute] int cod_obra)
        {
            try
            {
                return _context.Invoice.FromSqlRaw("exec facturas_int_c {0}, {1}, {2}, {3}", matricula, desde.ToString("MM-dd-yy"), hasta.ToString("MM-dd-yy"), cod_obra).AsEnumerable().ToList();
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("accreditations/{matricula}/{desde}/{hasta}/{orden}")]
        public List<Accreditation> GetAccreditations([FromRoute] int matricula, [FromRoute] DateTime desde, [FromRoute] DateTime hasta, [FromRoute] int orden)
        {
            try
            {
                return _context.Accreditation.FromSqlRaw("exec acreditaciones_periodo_op_c {0}, {1}, {2}, {3}", matricula, desde.ToString("MM-dd-yy"), hasta.ToString("MM-dd-yy"), orden).AsEnumerable().ToList();
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("detail/{tipo}/{letra}/{cod}/{nro}")]
        public List<InvoiceDetail> GetInvoiceDetails([FromRoute] string tipo, [FromRoute] string letra, [FromRoute] string cod, [FromRoute] string nro)
        {
            try
            {
                List<InvoiceDetail> list = new List<InvoiceDetail>();

                list = _context.InvoiceDetail.FromSqlRaw("exec facturas_detalle_c {0}, {1}, {2}, {3}", tipo, letra, cod, nro).AsEnumerable().ToList();

                List<InvoiceDetailItem> items = new List<InvoiceDetailItem>();

                items = _context.InvoiceDetailItem.FromSqlRaw("exec facturas_detalle_c1 {0}, {1}, {2}, {3}", tipo, letra, cod, nro).AsEnumerable().ToList();

                foreach (InvoiceDetail invoiceDetail in list)
                {
                    invoiceDetail.imagenes = GetImagesByOrden(invoiceDetail.nro_orden);
                    invoiceDetail.items = items.Where(ele => ele.nro_orden == invoiceDetail.nro_orden).ToList();
                }

                return list;
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("detail/items/{tipo}/{letra}/{cod}/{nro}")]
        public List<InvoiceDetailItem> GetInvoiceDetailItems([FromRoute] string tipo, [FromRoute] string letra, [FromRoute] string cod, [FromRoute] string nro)
        {
            try
            {
                List<InvoiceDetailItem> list = new List<InvoiceDetailItem>();

                list = _context.InvoiceDetailItem.FromSqlRaw("exec facturas_detalle_c1 {0}, {1}, {2}, {3}", tipo, letra, cod, nro).AsEnumerable().ToList();

                return list;
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("images/{tipo}/{letra}/{cod}/{nro}")]
        public StringResult GetImages([FromRoute] string tipo, [FromRoute] string letra, [FromRoute] string cod, [FromRoute] string nro)
        {
            try
            {
                string path = "./PDF/Imagenes/";
                string zippath = path + "zip";
                string zipfile = zippath + "/" + tipo + letra + cod + nro + ".zip";

                if (!System.IO.File.Exists(zipfile))
                {
                    var zip = ZipFile.Open(zipfile, ZipArchiveMode.Create);

                    List<Images> images = _context.Images.FromSqlRaw("exec facturas_imagenes_c {0}, {1}, {2}, {3}", tipo, letra, cod, nro).AsEnumerable().ToList();

                    foreach (Images fileName in images)
                    {
                        var imgPath = "./" + fileName.url;
                        zip.CreateEntryFromFile(imgPath, Path.GetFileName(imgPath), CompressionLevel.Optimal);
                    }

                    zip.Dispose();
                }


                StringResult sr = new StringResult();
                var location = new Uri($"{Request.Scheme}://{Request.Host}");

                sr.value = location.AbsoluteUri + "PDF/Imagenes/zip/" + tipo + letra + cod + nro + ".zip";

                return sr;

            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        public List<ImagenOrden> GetImagesByOrden(int orden)
        {
            List<ImagenOrden> list = _context.ImagenOrden.FromSqlRaw("exec orden_imagen_c {0}", orden).AsEnumerable().ToList();

            return list;
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("images/download/{matricula}/{tipo}/{letra}/{cod}/{nro}")]
        public ActionResult DownloadImages([FromRoute] int matricula, [FromRoute] string tipo, [FromRoute] string letra, [FromRoute] string cod, [FromRoute] string nro)
        {
            List<Images> images = _context.Images.FromSqlRaw("exec facturas_imagenes_c {0}, {1}, {2}, {3}", tipo, letra, cod, nro).AsEnumerable().ToList();

            string path = "./Files/Invoices/" + matricula.ToString();
            string invoicepath = path + "/" + tipo + letra + cod + nro;
            string zippath = invoicepath + "/zip";

            createDir(path);
            createDir(invoicepath);
            createDir(zippath);

            int last_orden = 0;
            int i = 0;

            foreach (var file in images)
            {
                if (last_orden != file.nro_orden)
                {
                    i = 1;
                    last_orden = file.nro_orden;
                }
                else
                {
                    i++;
                }

                string fileName = invoicepath + "/" + tipo + letra + cod + nro + "_" + file.nro_orden.ToString() + "_" + i.ToString() + Path.GetExtension(file.url);

                WebClient webClient = new WebClient();

                webClient.DownloadFile(file.url, fileName);
            }

            string zipfile = zippath + "/" + tipo + letra + cod + nro + ".zip";

            var zip = ZipFile.Open(zipfile, ZipArchiveMode.Create);

            var fileEntries = Directory.GetFiles(invoicepath).Where(name => name.EndsWith(".pdf"));

            foreach (string fileName in fileEntries)
            {
                zip.CreateEntryFromFile(fileName, Path.GetFileName(fileName), CompressionLevel.Optimal);
            }

            zip.Dispose();


            return Ok();
        }

        private void createDir(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("accreditations/bancos/{matricula}/{desde}/{hasta}")]
        public List<BankAccreditation> GetBankAccreditation([FromRoute] int matricula, [FromRoute] DateTime desde, [FromRoute] DateTime hasta)
        {
            try
            {
                return _context.BankAccreditation.FromSqlRaw("exec acreditaciones_periodo_c {0}, {1}, {2}", matricula, desde.ToString("MM-dd-yy"), hasta.ToString("MM-dd-yy")).AsEnumerable().ToList();
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("accreditations/gastos/{tipo}/{letra}/{cod}/{nro}")]
        public List<Gastos> GetGastos([FromRoute] string tipo, [FromRoute] string letra, [FromRoute] string cod, [FromRoute] string nro)
        {
            try
            {
                return _context.Gastos.FromSqlRaw("exec gastos_op_c {0}, {1}, {2}, {3}", tipo, letra, cod, nro).AsEnumerable().ToList();
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("os/{matricula}")]
        public List<ObraSocial> GetObrasSociales([FromRoute] int matricula)
        {
            try
            {
                return _context.ObraSocial.FromSqlRaw("exec gerenciadora_obras {0}", matricula).AsEnumerable().ToList();
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("os/all/{matricula}")]
        public List<OS> GetAllObrasSociales([FromRoute] int matricula)
        {
            try
            {
                return _context.OS.FromSqlRaw("exec obras_sociales_c {0}", matricula).AsEnumerable().ToList();
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("osgeren")]
        public List<OSGeren> GetOSGeren()
        {
            try
            {
                return _context.OSGeren.FromSqlRaw("exec obras_geren_c").AsEnumerable().ToList();
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("convenios/{cod_geren}/{cod_obra}")]
        public List<Convenio> GetConvenios([FromRoute] int cod_geren, [FromRoute] int cod_obra)
        {
            try
            {
                return _context.Convenio.FromSqlRaw("exec convenios_c1 {0}, {1}", cod_geren, cod_obra).AsEnumerable().ToList();
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("prestador/all/{matricula}")]
        public List<Prestador> GetAllPrestadores([FromRoute] int matricula)
        {
            try
            {
                return _context.Prestador.FromSqlRaw("exec prestadores_c {0}", matricula).AsEnumerable().ToList();
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("ordenes/fecha/{matricula}/{fec_ini}/{fec_fin}/{detalle}/{tipo}/{matgrupo}")]
        public List<OrdenPagada> GetOrdersByDate([FromRoute] int matricula, [FromRoute] DateTime fec_ini, [FromRoute] DateTime fec_fin, [FromRoute] bool detalle, [FromRoute] int tipo, [FromRoute] int matgrupo)
        {
            try
            {
                if (tipo == -1 || matgrupo == -1)
                {
                    return _context.OrdenPagada.FromSqlRaw("exec ordenes_pagadas_c {0}, {1}, {2}, null, null, null, {3}, null, null", matricula, fec_ini.ToString("MM-dd-yy"), fec_fin.ToString("MM-dd-yy"), detalle).AsEnumerable().ToList();
                }
                else 
                {
                    return _context.OrdenPagada.FromSqlRaw("exec ordenes_pagadas_c {0}, {1}, {2}, null, null, null, {3}, {4}, {5}", matricula, fec_ini.ToString("MM-dd-yy"), fec_fin.ToString("MM-dd-yy"), detalle, tipo, matgrupo).AsEnumerable().ToList();
                }
                
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("ordenes/paciente/{matricula}/{paciente}/{detalle}/{tipo}/{matgrupo}")]
        public List<OrdenPagada> GetOrdersByPaciente([FromRoute] int matricula, [FromRoute] string paciente, [FromRoute] bool detalle, [FromRoute] int tipo, [FromRoute] int matgrupo)
        {
            try
            {
                if (tipo == -1 || matgrupo == -1)
                {
                    return _context.OrdenPagada.FromSqlRaw("exec ordenes_pagadas_c {0}, null, null, {1}, null, null, {2}", matricula, paciente, detalle).AsEnumerable().ToList();
                }
                else
                {
                    return _context.OrdenPagada.FromSqlRaw("exec ordenes_pagadas_c {0}, null, null, {1}, null, null, {2}, {3}, {4}", matricula, paciente, detalle, tipo, matgrupo).AsEnumerable().ToList();
                }
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("ordenes/os/{matricula}/{fec_ini}/{fec_fin}/{cod_obra}/{detalle}/{tipo}/{matgrupo}")]
        public List<OrdenPagada> GetOrdersByObraSocial([FromRoute] int matricula, [FromRoute] DateTime fec_ini, [FromRoute] DateTime fec_fin, [FromRoute] int cod_obra, [FromRoute] bool detalle, [FromRoute] int tipo, [FromRoute] int matgrupo)
        {
            try
            {
                if (tipo == -1 || matgrupo == -1)
                {
                    if (cod_obra == -1)
                    {
                        return _context.OrdenPagada.FromSqlRaw("exec ordenes_pagadas_c {0}, {1}, {2}, null, null, null, {3}", matricula, fec_ini.ToString("MM-dd-yy"), fec_fin.ToString("MM-dd-yy"), detalle).AsEnumerable().ToList();
                    }
                    else
                    {
                        return _context.OrdenPagada.FromSqlRaw("exec ordenes_pagadas_c {0}, {1}, {2}, null, null, {3}, {4}", matricula, fec_ini.ToString("MM-dd-yy"), fec_fin.ToString("MM-dd-yy"), cod_obra, detalle).AsEnumerable().ToList();
                    }
                }
                else
                {
                    if (cod_obra == -1)
                    {
                        return _context.OrdenPagada.FromSqlRaw("exec ordenes_pagadas_c {0}, {1}, {2}, null, null, null, {3}, {4}, {5}", matricula, fec_ini.ToString("MM-dd-yy"), fec_fin.ToString("MM-dd-yy"), detalle, tipo, matgrupo).AsEnumerable().ToList();
                    }
                    else
                    {
                        return _context.OrdenPagada.FromSqlRaw("exec ordenes_pagadas_c {0}, {1}, {2}, null, null, {3}, {4}, {5}, {6}", matricula, fec_ini.ToString("MM-dd-yy"), fec_fin.ToString("MM-dd-yy"), cod_obra, detalle, tipo, matgrupo).AsEnumerable().ToList();
                    }
                }
                
                
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("ordenes/prestador/{matricula}/{fec_ini}/{fec_fin}/{cod_pres}/{detalle}/{tipo}/{matgrupo}")]
        public List<OrdenPagada> GetOrdersByPrestador([FromRoute] int matricula, [FromRoute] DateTime fec_ini, [FromRoute] DateTime fec_fin, [FromRoute] int cod_pres, [FromRoute] bool detalle, [FromRoute] int tipo, [FromRoute] int matgrupo)
        {
            try
            {
                if (tipo == -1 || matgrupo == -1)
                {
                    if (cod_pres == -1)
                    {
                        return _context.OrdenPagada.FromSqlRaw("exec ordenes_pagadas_c {0}, {1}, {2}, null, null, null, {3}", matricula, fec_ini.ToString("MM-dd-yy"), fec_fin.ToString("MM-dd-yy"), detalle).AsEnumerable().ToList();
                    }
                    else
                    {
                        return _context.OrdenPagada.FromSqlRaw("exec ordenes_pagadas_c {0}, {1}, {2}, null, {3}, null, {4}", matricula, fec_ini.ToString("MM-dd-yy"), fec_fin.ToString("MM-dd-yy"), cod_pres, detalle).AsEnumerable().ToList();
                    }
                }
                else
                {
                    if (cod_pres == -1)
                    {
                        return _context.OrdenPagada.FromSqlRaw("exec ordenes_pagadas_c {0}, {1}, {2}, null, null, null, {3}, {4}, {5}", matricula, fec_ini.ToString("MM-dd-yy"), fec_fin.ToString("MM-dd-yy"), detalle, tipo, matgrupo).AsEnumerable().ToList();
                    }
                    else
                    {
                        return _context.OrdenPagada.FromSqlRaw("exec ordenes_pagadas_c {0}, {1}, {2}, null, {3}, null, {4}, {5}, {6}", matricula, fec_ini.ToString("MM-dd-yy"), fec_fin.ToString("MM-dd-yy"), cod_pres, detalle, tipo, matgrupo).AsEnumerable().ToList();
                    }
                }
                
                
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("documentos/")]
        public List<TipoDocumento> GetTiposDeDocumentos()
        {
            try
            {
                return _context.TipoDocumento.FromSqlRaw("exec archivos_cab_c").AsEnumerable().ToList();
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("downloads/{matricula}/{tipo}/{yyyymm}")]
        public List<Models.File> getDowloadFiles([FromRoute] int matricula, [FromRoute] int tipo, [FromRoute] string yyyymm)
        {
            try
            {
                List<ArchivoDownload> list = _context.ArchivoDownload.FromSqlRaw("exec archivos_det_c {0}", tipo).AsEnumerable().ToList();

                List<Models.File> result = new List<Models.File>();

                if (yyyymm.Length == 4)
                {
                    for (int i = 0; i <=12; i++)
                    {
                        result.AddRange(GetFiles(matricula, yyyymm + i.ToString("00"), list, true));
                    }
                }
                else
                {
                    result.AddRange(GetFiles(matricula, yyyymm, list, true));
                }
                

                return result;

            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        private List<Models.File> GetFiles(int matricula, string yyyymm, List<ArchivoDownload> list, Boolean byyear)
        {
            string path = "./PDF/" + yyyymm + "/";

            if (!Directory.Exists(path))
            {
                return new List<Models.File>();
            }

            string[] fileEntries = Directory.GetFiles(path);
            List< Models.File> result = new List<Models.File>();


            foreach (string fileName in fileEntries)
            {
                foreach (ArchivoDownload item in list)
                {
                    string name = Path.GetFileName(fileName);

                    if (byyear)
                    {
                        if (name.IndexOf(yyyymm.Substring(0,4), 0) != -1 && name.ToUpper().Trim().IndexOf(item.tipo.ToUpper().Trim() + "-", 0) != -1 && name.IndexOf("-"+matricula.ToString().PadLeft(6,'0'), 0) != -1)
                        {
                            Models.File file = new Models.File();
                            file.path = fileName;
                            file.nombre = Path.GetFileName(fileName);
                            file.descripcion = item.nombre;
                            result.Add(file);
                            break;
                        }
                    }
                    else
                    {
                        if (name.IndexOf(yyyymm, 0) != -1 && name.ToUpper().IndexOf(item.tipo.ToUpper().Trim() + "-", 0) != -1 && name.IndexOf("-"+matricula.ToString().PadLeft(6, '0'), 0) != -1)
                        {
                            Models.File file = new Models.File();
                            file.path = fileName;
                            file.nombre = Path.GetFileName(fileName);
                            file.descripcion = item.nombre;
                            result.Add(file);
                            break;
                        }
                    }
                }
            }

            return result;
        }

        [Microsoft.AspNetCore.Mvc.HttpPost("pdf/")]
        public FileStreamResult GetPdf([Microsoft.AspNetCore.Mvc.FromBody] PdfRequest request)
        {
            FileStream pdf = new FileStream(request.url, FileMode.Open);
            return new FileStreamResult(pdf, "application/pdf");

        }
    }

}