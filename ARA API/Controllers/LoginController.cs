﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARA_API.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ARA_API.Controllers
{
    [EnableCors("AllowAll")]
    [Route("[controller]")]
    [ApiController]
    public class LoginController : Controller
    {
        private Context _context;

        public LoginController(Context context)
        {
            _context = context;
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("intentos/{matricula}/{tipo_contador}")]
        public IntentosResult GetIntentos([FromRoute] int matricula, [FromRoute] string tipo_contador)
        {
            return _context.IntetosResult.FromSqlRaw("exec login_intentos_c {0}, {1}", matricula, tipo_contador).AsEnumerable().FirstOrDefault();
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("valida/{matricula}/{password}/{tipo_contador}")]
        public ValidaResult ValidaWithTipoContador([FromRoute] int matricula, [FromRoute] string password, [FromRoute] string tipo_contador)
        {
            try
            {
                return _context.ValidaResult.FromSqlRaw("exec login_valida_c {0}, {1}, {2}", matricula, password, tipo_contador).AsEnumerable().FirstOrDefault();
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("valida/{matricula}/{password}")]
        public ValidaResult Valida([FromRoute] int matricula, [FromRoute] string password)
        {
            try
            {
                return _context.ValidaResult.FromSqlRaw("exec login_valida_c {0}, {1}", matricula, password).AsEnumerable().FirstOrDefault();
            }
            catch (InvalidOperationException exc)
            {
                throw new InvalidOperationException();
            }
        }

        [Microsoft.AspNetCore.Mvc.HttpPost("change")]
        public BoolResult Change([Microsoft.AspNetCore.Mvc.FromBody] ChangeRequest request)
        {
            return _context.BoolResult.FromSqlRaw("exec login_cambio_pswd {0}, {1}, {2}, {3}", request.Matricula, request.Password, request.NewPassword, request.TipoContador.ToUpper()).AsEnumerable().FirstOrDefault();

        }
    }
}